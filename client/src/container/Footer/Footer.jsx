import React from "react";
import { useState } from "react";
import { images } from "../../constants";
import { AppWrap, SectionWrap } from "../../wrapper";

import { client } from "../../sanityClient";

import "./Footer.scss";

const Footer = () => {
  const [formData, setFormData] = useState({
    fullName: "",
    email: "",
    message: "",
  });

  const [isFormSubmitted, setIsFormSubmitted] = useState(false);
  const [loading, setLoading] = useState(false);

  const { fullName, email, message } = formData;

  const handleInputChange = (e) => {
    const { name, value } = e.target;

    setFormData({ ...formData, [name]: value });
    return;
  };

  const handleSubmit = (e) => {
    setLoading(true);

    const contact = {
      _type: "contact",
      fullName,
      email,
      message,
    };
    client.create(contact).then(() => {
      setLoading(false);
      setIsFormSubmitted(true);
    });
  };

  return (
    <>
      <h2 className="head-text">Have a chat with me.</h2>
      <div className="app__footer-cards">
        <div className="app__footer-card">
          <img src={images.email} alt="email" />
          <a href="mailto:isaac@isaacj.me" className="p-text">
            isaac@isaacj.me
          </a>
        </div>
        <div className="app__footer-card">
          <img src={images.mobile} alt="phone" />
          <a href="tel: +44 7360 760 744" className="p-text">
            +44 7360 760 744
          </a>
        </div>
      </div>
      {!isFormSubmitted ? (
        <div className="app__footer-form app__flex">
          <div className="app__flex">
            <input
              type="text"
              className="p-text"
              placeholder="Your Name"
              value={fullName}
              onChange={handleInputChange}
              name="fullName"
            />
          </div>
          <div className="app__flex">
            <input
              type="text"
              className="p-text"
              placeholder="Your Email"
              value={email}
              onChange={handleInputChange}
              name="email"
            />
          </div>
          <div>
            <textarea
              name="message"
              className="p-text"
              value={message}
              placeholder="Your Message"
              onChange={handleInputChange}
            ></textarea>
          </div>
          <button type="button" className="p-text" onClick={handleSubmit}>
            {loading ? "Sending" : "Send Message"}
          </button>
        </div>
      ) : (
        <div>
          <h3 className="head-text">Thank Your For Contacting Me.</h3>
        </div>
      )}
    </>
  );
};

export default AppWrap(
  SectionWrap(Footer, "app__footer"),
  "contact",
  "app__greybg"
);
