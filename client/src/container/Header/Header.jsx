import React from "react";
import { motion } from "framer-motion";

import { images } from "../../constants";

import { AppWrap } from "../../wrapper";

import "./Header.scss";

const Header = () => {
  const scaleVariants = {
    whileInView: {
      scale: [0, 1],
      opacity: [0, 1],
      transition: {
        duration: 1,
        ease: "easeInOut",
      },
    },
  };

  const skillsCircles = [images.flutter, images.redux, images.sass];
  return (
    <div className="app__header app__flex">
      <motion.div
        whileInView={{ x: [-100, 0], opacity: [0, 1] }}
        transition={{ duration: 0.5 }}
        className="app__header-info"
      >
        <div className="app__header-badge">
          <div className="badge-cmp app__flex">
            <span>👋</span>
            <div style={{ marginLeft: 20 }}>
              <p className="p-text">Hi, my name is</p>
              <h1 className="head-text">Isaac</h1>
            </div>
          </div>

          <div className="tag-cmp app__flex">
            <p className="p-text">Fullstack Developer</p>
            <p className="p-text">Mobile Engineer (IOS, Android)</p>
          </div>
        </div>
      </motion.div>

      <motion.div
        whileInView={{ opacity: [0, 1] }}
        transition={{ duration: 0.5, delayChildren: 0.5 }}
        className="app__header-img"
      >
        <img src={images.profile_} alt="Profile_bg" />
        <motion.img
          whileInView={{ scale: [0, 1] }}
          transition={{ duration: 1, ease: "easeInOut" }}
          src={images.circle}
          alt="Profile_circle"
          className="overlay_circle"
        />
      </motion.div>

      <motion.div
        variants={scaleVariants}
        whileInView={scaleVariants.whileInView}
        className="app__header-circles"
      >
        {skillsCircles.map((skill, index) => (
          <div className="circle-cmp app__flex" key={`circle-${index}`}>
            <img src={skill} alt={`${skill}-circle`} />
          </div>
        ))}
      </motion.div>
    </div>
  );
};

export default AppWrap(Header, "home");
