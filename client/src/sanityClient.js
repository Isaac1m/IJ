import sanityClient from "@sanity/client";

import imageUrlBuilder from "@sanity/image-url";

export const client = sanityClient({
  projectId: process.env.REACT_APP_SANITY_PROJECT_ID,
  token: process.env.REACT_APP_SANITY_TOKEN,
  apiVersion: process.env.REACT_APP_SANITY_API_VERSION,
  useCdn: true,
  dataset: process.env.REACT_APP_SANITY_TOKEN_DATASET,
  ignoreBrowserTokenWarning: true,
});

export const builder = imageUrlBuilder(client);

export const urlFor = (source) => builder.image(source);
